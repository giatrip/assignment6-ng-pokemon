# Pokemon Trainer Angular

Submission for Assignment 3 "Create a pokemon trainer using Angular"

As per Appendix A the webpage for creating a pokemon trainer was created. 
All functionality and documentation required has been met. Details bellow. 

# Summary

The aim of the assignment was to create a webpage using Angular where you login with your username and then you can choose what pokemon
you want to catch or release amongst many pokemon that are displayed with their name and their image. Then the caught pokemon are displayed 
in your profile.

# What we used

We used heroku so as to save our information and Visual Studio Code along with Angular to develop the webpage.

## What we learned

Working with Angular was a very fun experience. As we had previously worked with React there were some things that
reminded us of React but there were also many new things to learn as well which made working with Angular a very interesting
experience and we had a lot of gains from working on it.

#### App
- Download/Clone the repo. 
- Open the solution in your Visual Studio Code environment.
- Run the application with ng serve
- Depending on which API you want to work you need to change the URL and the KEY in the environment.ts and environment.prod.ts file

## Authors / Contributors

- **Aris Kardasis** [ArisKard](https://github.com/@ArisKard) (Aris Kardasis on moodle)
- **Giannis Tripodis** [giatrip](https://gitlab.com/giatrip) (Giannis Tripodis on moodle)
- **Bill Koutsis** [Vasikout](https://www.github.com/@Vasikout) (Vasilis Koutsis on Moodle)

## Hosting platform ( Vercel ) [pokemon-site](https://assignment6-ng-pokemon.vercel.app/)