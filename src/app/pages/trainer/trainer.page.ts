import { Component } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage {

  get loading() : boolean {
    return this.pokemonService.loading
  }

  get name(): string{
    if (this.trainerService.trainer) {
      return this.trainerService.trainer?.username
    }
    return ""
  }

  get count() : number {
    if (this.trainerService.trainer) {
      return this.trainerService.trainer.pokemon.length
  }
  return 0
}

  get pokemons() : Pokemon[] {
    if (this.trainerService.trainer) {
      return  this.trainerService.trainer?.pokemon
    }
    return []
  } 

  constructor(
    private readonly trainerService: TrainerService,
    private readonly pokemonService: PokemonCatalogueService
   ) {

  }

}
