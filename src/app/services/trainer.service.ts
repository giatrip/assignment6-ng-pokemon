import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  private _trainer?: Trainer;

  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  set trainer(trainer: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!);
    this._trainer = trainer;
  }

  constructor(private readonly router: Router) {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer)
  }

  public inCapture(pokemonId: number): boolean {
    if (this._trainer) {
      return Boolean(this._trainer?.pokemon.find((pokemon: Pokemon) => pokemon.id == pokemonId))
    }

    return false
  }

  public capturePokemon(pokemon: Pokemon): void {
    if (this._trainer) {
      this._trainer.pokemon.push(pokemon)
    }
  }

  public releasePokemon(pokemonId: number): void {
    if (this._trainer) {
      this._trainer.pokemon = this._trainer.pokemon.filter((pokemon: Pokemon) => pokemon.id !== pokemonId);
    }

  }

  public logout(): void {
    sessionStorage.removeItem('pokemons-trainer')
    this.router.navigateByUrl("/login")
  }

}
