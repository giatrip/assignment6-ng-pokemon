import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { enviroment } from 'enviroments/enviroment';
import { Observable, tap } from 'rxjs';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { TrainerService } from './trainer.service';

const { apiKey, apiTrainers} = enviroment

@Injectable({
  providedIn: 'root'
})
export class CaptureService {

  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonCatalogueService,
    private readonly trainerService: TrainerService 
     
    
  ) { }

  public addToCapture(pokemonId : number): Observable<Trainer>{
    if (!this.trainerService.trainer){
      throw new Error("addToCapture: There is no trainer");
    }

    const trainer: Trainer = this.trainerService.trainer;
    const pokemon: Pokemon | undefined = this.pokemonService.pokemonById(pokemonId);

    if (!pokemon){
      console.log(pokemon)
      throw new Error("addToCapture: No pokemon with id: " + pokemonId)
    }

    if (this.trainerService.inCapture(pokemonId)){
      this.trainerService.releasePokemon(pokemonId)
    } else {
      this.trainerService.capturePokemon(pokemon)
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })

     return this.http.patch<Trainer>(`${apiTrainers}/${trainer.id}`,{
      pokemon: [...trainer.pokemon]
     },{
      headers
     })
     .pipe(
      tap((updatedTrainer: Trainer) => {
        this.trainerService.trainer= updatedTrainer;
      })
     )
  }
  
}
