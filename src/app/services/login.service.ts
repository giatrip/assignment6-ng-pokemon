import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { enviroment } from 'enviroments/enviroment';
import { map, Observable, of, switchMap} from 'rxjs';
import { Trainer } from '../models/trainer.model';

const { apiTrainers, apiKey } = enviroment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  

  constructor(private readonly http: HttpClient) { }

  public login(username: string): Observable<Trainer> {
  
    return this.checkUsername(username)
      .pipe(
        switchMap((trainer: Trainer | undefined) => {
          if (trainer === undefined) {
            return this.createTrainer(username);
          }
          return of(trainer)
        })
      )

  }

  // Login


  // Check if the trainer exists
  private checkUsername(username: string): Observable<Trainer | undefined> {
    return this.http.get<Trainer[]>(`${apiTrainers}?username=${username}`)
      .pipe(
        map((response: Trainer[]) => response.pop())
      )
  }

  // Create a Trainer
  private createTrainer(username: string): Observable<Trainer> {
    const trainer = {
      username: username,
      pokemon: []
    }

    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    })

    return this.http.post<Trainer>(apiTrainers, trainer, {
      headers
    })
  }
}
