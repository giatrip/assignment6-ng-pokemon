import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { enviroment } from 'enviroments/enviroment';
import { finalize } from 'rxjs';
import { data } from '../models/data.model';
import { Pokemon } from '../models/pokemon.model';

const { apiPokemons } = enviroment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _pokemons: Pokemon[] = [];
  private _error: string = "";
  private _loading: boolean = false;

  get pokemons(): Pokemon[] {
    return this._pokemons
  }

  get error(): string{
    return this._error
  }

  get loading(): boolean{
    return this._loading
  }
  
  constructor(private readonly http: HttpClient) { }

  public findAllPokemons(): void{

    if (this._pokemons.length>0 || this._loading) {
      return;
    }

    this._loading = true;
    this.http.get<data>(apiPokemons)
      .pipe(
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (data : data ) => {
          data.results.map((pokemon, index) =>{
            this._pokemons.push(pokemon)
            // this._pokemons[index].name=pokemon.name
            this._pokemons[index].id=index+1
            this._pokemons[index].img=`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${index+1}.png`
          } )

        },
        error: (error: HttpErrorResponse) => {
          this._error= error.message;
        }
      })
    }

    public pokemonById(id: number): Pokemon | undefined {
      return this._pokemons.find((pokemon: Pokemon) => pokemon.id===id)
    }

}


