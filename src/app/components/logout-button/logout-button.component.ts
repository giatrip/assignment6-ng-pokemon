import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-logout-button',
  templateUrl: './logout-button.component.html',
  styleUrls: ['./logout-button.component.css']
})
export class LogoutButtonComponent implements OnInit {

  constructor(
    private readonly trainerService: TrainerService,
    private readonly loginService: LoginService
  ) {}
  
  ngOnInit(): void {
    
  }

  onLogout() : void {
    this.trainerService.logout()
  }


}
