import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CaptureService } from 'src/app/services/capture.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-capture-pokemon-button',
  templateUrl: './capture-pokemon-button.component.html',
  styleUrls: ['./capture-pokemon-button.component.css']
})
export class CapturePokemonButtonComponent implements OnInit {

  public loading: boolean = false;
  public isCaptured: boolean = false

  @Input() pokemonId : number = 0;
  
  constructor(
    private readonly trainerService: TrainerService,
    private readonly captureService: CaptureService
  ) {}
  
  ngOnInit(): void {
    this.isCaptured=this.trainerService.inCapture(this.pokemonId);
  }

  onCapturePokemon(): void {
    this.loading=true;
    //add pokemon to trainer
    this.captureService.addToCapture(this.pokemonId)
      .subscribe({
        next: (trainer: Trainer) => {
          this.loading=false;
          this.isCaptured = this.trainerService.inCapture(this.pokemonId)
        },
        error: (error: HttpErrorResponse) => {
          console.log("ERROR", error.message)
        }
      })
  }

}
