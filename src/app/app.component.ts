import { Component, OnInit } from '@angular/core';
import { PokemonCatalogueService } from './services/pokemon-catalogue.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'pokemon';

  constructor (
    private readonly pokemonCatalogueService : PokemonCatalogueService
  ) {}

  ngOnInit() {
    this.pokemonCatalogueService.findAllPokemons();
  }
}
